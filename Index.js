const Discord = require("discord.js")
const config = require("./config.json")
const publicIp = require('public-ip');

const commandHello = require("./commands/Hello.js")
const commandTest = require("./commands/Test.js")
const commandTournament = require("./commands/Tournament")
const log = require("./commands/SendLog")

const client = new Discord.Client()

let prefix // The prefix of the command
let botToken
let lastIndexAdd = 0
let lastIndexRemove = 0

/** On connecting bot event **/
client.on("ready", async _ => {
    console.log(`Logged in as ${client.user.tag}!`)
    await log.sendLog(client, log.level.SUCCESS, `Logged in as ${client.user.tag}!`)
    if (await IsDebug()) {
        prefix = "!"
        console.log("Debug")
        await log.sendLog(client, log.level.INFO, "Debug")
    } else {
        prefix = "+"
        console.log("Production")
        await log.sendLog(client, log.level.INFO, "Production")
    }
    client.user.setPresence({
        status: 'online',
        activity: {
            name: await IsDebug() ? "Coding the bot" : "War Thunder",
            type: "PLAYING",
        }})
        .then(presence => {console.log(`Activity set to "${presence.activities[0].name}"`); log.sendLog(client, log.level.INFO, `Activity set to "${presence.activities[0].name}"`)})
        .catch(console.error)
    commandTournament.tournament(client).catch(err => console.error(err))
})

/**
 * On message event
 */
client.on("message",async message => {

    if (message.author.bot) return
    if (!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length)
    const args = commandBody.split(' ')
    const command = args.shift().toLowerCase()

    if (command === "hello") // On the command : your prefix + 'hello'
        await commandHello.hello(client, message)

    else if (command === "test")
        commandTest.test(message, args)

    else if (command === "tss") {
        if (message.author.id === config.USER_ID_RAILO)
            if (args[0] === "once")
                commandTournament.getOnceNewTournaments(args[0], message).catch(err => console.error(err))
            else if (args[0] === "auto") {
                if (args[1] === "start")
                    commandTournament.isTournamentAuto(true).catch(err => message.reply(err.message).catch(err => console.error(err)))
                else if (args[1] === "stop")
                    commandTournament.isTournamentAuto(false).catch(err => message.reply(err.message).catch(err => console.error(err)))
                else
                    message.reply("You argument is wrong, pls check it.").catch(err => console.error(err))
            }
            else
                message.reply("This is the tournament command, pls check your argument.").catch(err => console.error(err))
        else
            message.reply("You can't do that sir.").catch(err => console.error(err))
    }
})

/**
 * On "guildMemberAdd" event
 */
client.on("guildMemberAdd", async member => {
    const typeUser = member.user.bot ? "Bot" : "User"
    console.log(`New ${typeUser} "${member.user.tag}" has joined "${member.guild.name}"`)
    await log.sendLog(client, log.level.INFO, `New ${typeUser} "${member.user.tag}" has joined "${member.guild.name}"`)
    if (member.guild.id === config.SERVER_ID_LYS) {
        if (member.user.bot) {
            const roleBot = member.guild.roles.cache.find(role => role.id === config.ROLE_ID_LYS_BOT)
            member.roles.add(roleBot)
                .then(log.sendLog(client, log.level.INFO, `Bot "${member.user.tag}" has get the role "${roleBot.name}"`))
                .catch(err => {console.log(err.message); log.sendLog(client, log.level.ERROR, `${err.message}`, "Add role on guildMemberAdd")})
        }
        else {
            const roleInvite = member.guild.roles.cache.find(role => role.id === config.ROLE_ID_LYS_INVITE)
            member.roles.add(roleInvite)
                .then(log.sendLog(client, log.level.INFO, `User "${member.user.tag}" has get the role "${roleInvite.name}"`))
                .catch(err => {console.log(err.message); log.sendLog(client, log.level.ERROR, `${err.message}`, "Add role on guildMemberAdd")})
            await sendMessageAddRemove(member.user, true)
        }
    }
    else if (member.guild.id === config.SERVER_ID_GROUPE_INCUBATOR_2000) {
        client.channels.cache.get(config.CHANNEL_ID_general).send(`Bienvenue ${member.user} dans ce serveur de barge`)
        const role = member.guild.roles.cache.find(role => role.id === "821288147735543839")
        member.roles.add(role).catch(err => console.log(err.message))
    }
})

client.on("guildMemberRemove", async member => {
    const typeUser = member.user.bot ? "Bot" : "User"
    console.log(`The ${typeUser} "${member.user.tag}" has left "${member.guild.name}"`)
    await log.sendLog(client, log.level.INFO, `The ${typeUser} "${member.user.tag}" has left "${member.guild.name}"`)
    if (member.guild.id === config.SERVER_ID_LYS) {
        await sendMessageAddRemove(member.user, false)
    }
    else if (member.guild.id === config.SERVER_ID_GROUPE_INCUBATOR_2000) {
        client.channels.cache.get(config.CHANNEL_ID_general).send(`${member.user} au revoir et bon courage l'ami !`)
    }
})

const idkAdd = (user) => {
    const messagesForAddMember = [
        `:hugging: Bienvenue à ${user} dans l'escadron de la LYS ! Les membres sont un peu fous mais tu vas vite t'y faire :wink:`,
        `:hugging: Bienvenue ${user} dans ce serveur de barge. Et surtout bonne chance !`,
        `:hugging: ${user} voulait commander des pizzas mais il est tombé là. Bienvenue quand même à lui !`,
        `:hugging: ${user} viens d'arriver, APEROOOOO pour féter ça ! :beers:`,
        `:hugging: ${user} viens d'arriver, champaaaaagne ! :champagne: `
    ]
    let newIndex
    while (!newIndex || newIndex === lastIndexAdd) { newIndex = Math.floor(Math.random() * messagesForAddMember.length) }
    lastIndexAdd = newIndex
    return messagesForAddMember[newIndex]
}
const idkRemove = (user) => {
    const messagesForRemoveMember = [
        `:dash: Les racailles de la LYS on fait peur a ${user} ! Il a pris ses jambes à son cou !`,
        `:dash: ${user} au revoir et bon courage l'ami ! (Tu en auras besoin sans nous)`,
        `:dash: ${user} va nous manquer, petite coquine va.`,
        `:dash: ${user} a plus dans le bus :bus:`,
        `:dash: ${user} a plus tard dans la mare.`,
        `:dash: ${user} a bientôt en rendo.`,
        `:dash: ${user} a tout à l'heure dans ta soeur.`,
    ]
    let newIndex
    while (!newIndex || newIndex === lastIndexRemove) { newIndex = Math.floor(Math.random() * messagesForRemoveMember.length) }
    lastIndexRemove = newIndex
    return messagesForRemoveMember[newIndex]
}

const sendMessageAddRemove = async (user, isAdd) => {
    let message
    if (isAdd)
        message = idkAdd(user)
    else
        message = idkRemove(user)
    await client.channels.cache.get(config.CHANNEL_ID_registre_des_arrivees).send(message).catch(err => {throw err})
    await log.sendLog(client, log.level.INFO, `Message "${message}" sent successfully to channel "${client.channels.cache.find(c => c.id === config.CHANNEL_ID_registre_des_arrivees).name}" !`)
}

const IsDebug = async () => {

    const IPProd = "51.178.36.48"
    const IPV4 = await publicIp.v4()

    return !IPV4.toString().includes(IPProd);
}

(async _ => { return await IsDebug() ? config.BOT_TOKEN_WarthunderBot_Test : config.BOT_TOKEN_LYS })()
    .then( isDebug => client.login(isDebug)
        .then(console.log("Logged successfully"))
        .catch(err => console.log(err.message))
)
