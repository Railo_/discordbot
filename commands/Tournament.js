const config = require("../config.json")
const FormData = require('form-data')
const fetch = require("node-fetch")
const Discord = require("discord.js")
const log = require("./SendLog")


const LYS_Logo = new Discord.MessageAttachment('./assets/LYS_Logo.jpg')


class Tournament {

    /**
     * Object
     * @type {Discord.Client}
     */
    static #client//: Discord.Client

    /**
     * Logo of tournaments
     */
    static #logoPictures

    /**
     * Temp array of tournaments
     */
    static #tempTournaments

    /**
     * temp cookie for request
     * @type String
     */
    static #cookie

    /**
     * All Tournaments
     */
    static #tournaments

    /**
     * Array of IDs of Tournaments
     * @type Array<Number>
     */
    static #tournamentsIds

    /**
     * Array of IDs of Tournaments
     * @type Boolean
     */
    static #tournamentsAutoOn

    /**
     * Main function of Tournament class
     * @param {Discord.Client} client - Object
     */
    static async tournament( client ) {

        this.#client = client

        this.#tournamentsAutoOn = false

        this.#logoPictures = "https://static-tss.warthunder.com/icon_tournament/"

        this.#tempTournaments = []

        this.#tournamentsIds = []

        this.#cookie = "cf_clearance=71c80538d429eb26de7dbf2b6114c7cd59734670-1620679609-0-150;"

        await this.#getNewTournamentsAuto()
    }

    /**
     * Enable or disable tournament auto
     * @param {Boolean} response
     */
    static async isTournamentAuto(response) {
        this.#tournamentsAutoOn = response === true;
        let message = response ? "Launching of the daily engine for new tournaments !" : "Stop the daily engine for new tournaments !"
        await log.sendLog(this.#client, log.level.WARNING, message, "New Tournaments Auto")
        if (response === true)
            await this.#getNewTournaments()
    }

    /**
     * Every day, get new tournament
     * @returns {Promise<void>}
     */
    static async #getNewTournamentsAuto() {
        setInterval(async _ => {
            if (this.#tournamentsAutoOn === true) {
                await this.#getNewTournaments()
            }
        }, 86400000)
    }

    /**
     * Get once time, new tournaments
     * @param {string} cookie
     * @param {Discord.Message} message
     * @returns {Promise<void>}
     */
    static async getOnceNewTournaments(cookie, message) {
        if (cookie && cookie.includes("cf_clearance=")) {
            this.#cookie = cookie
            const that = this
            await that.#requestTournaments()
                .then( async _ => {
                    if (message)
                        await message.channel.send("**⚠ Warning ⚠ Once New Tournaments** : Starting the engine")
                    await that.sendMessageIntoDM("**⚠ Warning ⚠ Once New Tournaments** : Starting the engine", [config.USER_ID_RAILO]).catch(err => console.error(err))
                    await log.sendLog(this.#client, log.level.WARNING, "Once Starting the engine", "New Tournaments")
                    await that.#getNewTournaments()
                    })
                .catch( async err => {
                    if (message)
                        await message.channel.send("**❌ Error ❌ Once New Tournaments** : Request failed")
                    await log.sendLog(this.#client, log.level.ERROR, "Request failed :\n" + err.message, "Once New Tournaments")
                })
        }
        else {
            if (message)
                await message.channel.send("**❌ Error ❌ Once New Tournaments** : Cookie's format is wrong")
            await log.sendLog(this.#client, log.level.ERROR, "Cookie's format is wrong", "Once New Tournaments")
        }
    }

    /**
     * To get new tournaments of TSS WarThunder
     * @returns {Promise<void>}
     */
    static async #getNewTournaments() {
        // Send message when the method start
        await log.sendLog(this.#client, log.level.INFO, "Launch of the tournament engine", "New Tournaments")
        await this.#requestTournaments().catch(err => {console.error(err); log.sendLog(this.#client, log.level.ERROR, "Request failed\n" + err, "Request Tournaments"); })

        // Delete passed tournaments
        for( let i = 0; i < this.#tempTournaments.length; i++){
            if (!(this.#tempTournaments[i].status === "1" && this.#tempTournaments[i].active === "0") || this.#tournamentsIds.includes(this.#tempTournaments[i].tournamentID) || this.#tempTournaments[i].gameMode === "HB") {
                this.#tempTournaments.splice(i, 1);
                i--;
            }
        }

        // Sort by start date of tournaments
        this.#tempTournaments.sort(function compare(a, b) {
            if (a.dateStartTournament < b.dateStartTournament)
                return -1;
            if (a.dateStartTournament > b.dateStartTournament )
                return 1;
            return 0;
        });

        // Log the date of each tournament
        for (let tournament of this.#tempTournaments) {
            const date = new Date(tournament.dateStartTournament * 1000)
            console.log(tournament.gameMode + ", " + date + ", " + tournament.nameEN)
        }

        // Get details of each tournament
        for (let tournament of this.#tempTournaments)
            if (!tournament.details)
                await this.#requestDetailsTournament(tournament)
                    .catch(err => log.sendLog(this.#client, log.level.ERROR, `Error when requesting detail for tournament ${tournament.tournamentID} :\n${err.message}`, "Request Details Tournaments"))

        // Send tournament in a discord channel
        if (this.#tempTournaments.length > 0) {
            console.log(`${this.#tempTournaments.length} new tournament(s) get`)
            await log.sendLog(this.#client, log.level.SUCCESS, `${this.#tempTournaments.length} new tournament(s) get`, "New Tournaments")
            await this.#sendEmbedMessage(this.#tempTournaments)
        }
        else {
            console.log("No new tournament, no call of the method to send embed message")
            await log.sendLog(this.#client, log.level.WARNING, "No new tournament, no call of the method to send embed message", "New Tournaments")
        }
    }

    /**
     * Request to get all tournaments
     * @returns {Promise<void>} - true: the request worked, false: the request failed
     */
    static #requestTournaments = async () => {

        const myBody = new FormData();
        myBody.append("countCard", "0");
        myBody.append("action", "GetActiveTournaments");

        const requestOptions = {
            method: 'POST',
            headers: {
                //"cookie": "71c80538d429eb26de7dbf2b6114c7cd59734670-1620679609-0-150; PHPSESSID=vj4vhdmo0gk1mjncnpsua9s4b6; lang=en",
                "cookie": this.#cookie,
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36",
            },
            body: myBody,
        };

        await fetch("https://tss.warthunder.com/functions.php", requestOptions)
            .then(response => response.json())
            .then(result => this.#tempTournaments = result.data)
            .catch(err => {console.error(err); throw err});
    };

    /**
     * Request to get details for one tournament
     * @param {tempTournaments.any} tournament
     */
    static #requestDetailsTournament = async (tournament ) => {
        const requestOptions = {
            method: 'GET',
            headers: {
                "cookie": this.#cookie,
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36",
            },
            redirect: 'follow'
        };

        await fetch(`https://tss.warthunder.com/functions.php?id=${tournament.tournamentID}&action=DetailTournament`, requestOptions)
            .then(response => response.json())
            .then(result => tournament.details = result)
            .catch(err => {console.error(err.message); throw err});
    }

    /**
     * Send an embed message with data of each tournaments
     * @param {tempTournaments[]} tournaments
     * @returns {Promise<void>}
     */
    static async #sendEmbedMessage(tournaments) {
        let sentTournaments = []
        for (let tournament of tournaments) {
            const embed = this.#embedMessage(tournament)
            let channel

            switch (tournament.gameMode) {
                case 'AB' :
                    channel = config.CHANNEL_ID_tournoi_de_la_semaine_ab
                    break
                case 'RB' :
                    channel = config.CHANNEL_ID_tournoi_de_la_semaine_rb
                    break
                case 'HB' :
                    break
                default :
                    console.log("⚠ The game mode is not matching")
            }
            await this.#client.channels.cache.get(channel).send(embed)
                .then(_ => { this.#tournamentsIds.push(tournament.tournamentID); sentTournaments.push(tournament.gameMode + ", " + new Date(tournament.dateStartTournament * 1000).toLocaleString("en-GB", {timeZone:"UTC"}) + ", " + tournament.nameEN)})
                .catch(err => log.sendLog(this.#client, log.level.ERROR, err.message, "Send Embed Message"))
        }
        await log.sendLog(this.#client, log.level.SUCCESS, sentTournaments.join("\n"), "Send Embed Message")
    }

    /**
     * Return an embedMessage with values of the specific tournament
     * @param {tempTournaments.any} tournament
     * @returns {Discord.MessageEmbed}
     */
    static #embedMessage(tournament) {
        let color = 0x0099ff
        switch (tournament.details.teamSize.toString()) {
            case "1" :
                color = "#0c88db"
                break
            case "2" :
                color = "#50af1f"
                break
            case "3" :
                color = "#cdd41b"
                break
            case "5" :
                color = "#af1f21"
                break
            default :
                console.log("⚠ The game mode is not matching")
        }

        const embedMessage = {
            color: color,
            title: tournament.nameEN,
            url: `https://tss.warthunder.com/index.php?action=tournament&id=${tournament.tournamentID}`,
            author: {
                name: 'War Thunder Tournament',
                icon_url: 'https://tss.warthunder.com/images/menu/gaijin_top_panel_icon.png',
                url: 'https://tss.warthunder.com/index.php?action=current_tournament',
            },
            description: `${this.#timeStampToString(tournament.dateStartTournament)} UTC - ${this.#timeStampToString(tournament.dateEndTournament)} UTC`,
            thumbnail: {
                url: this.#logoPictures + tournament.details.icon_name,
            },
            fields: [
                {
                    name: '\u200b',
                    value: '\u200b',
                    inline: false,
                },
                {
                    name: 'Team Size',
                    value: `${tournament.details.teamSize}x${tournament.details.teamSize}`,
                    inline: true,
                },
                {
                    name: 'Type',
                    value: tournament.details.typeTournament,
                    inline: true,
                },
                {
                    name: 'Cluster',
                    value: tournament.details.cluster,
                    inline: true,
                },
                {
                    name: 'Prize pool',
                    value: tournament.details.prize_pool,
                    inline: false,
                },
                {
                    name: '\u200b',
                    value: '\u200b',
                    inline: false,
                },
            ],
            timestamp: new Date(tournament.dateStartTournament * 1000),
            footer: {
                text: 'Escadron de la LYS',
                icon_url: 'attachment://LYS_Logo.jpg',
            },
        }
        return { files: [LYS_Logo], embed: embedMessage}
    }

    /**
     * Get date format from timestamp format
     * @param {string} time - timestamp
     * @returns {string} - date string format
     */
    static #timeStampToString(time) {
        const options = { timeZone: "UTC", weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: "numeric", minute: "numeric" };
        const date = new Date(time * 1000)
        return date.toLocaleDateString('en-GB', options)
    }

    /**
     * Make timeStamp from human date
     * @param {string} time
     * @returns {number} - timeStamp format
     */
    static #dateToTimeStamp(time) {
        const tempDate = new Date(time)
        if (tempDate instanceof Date && !isNaN(tempDate)) {
            const newDate = new Date(Date.UTC( tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate()))
            return newDate / 1000
        }
        return 0
    }

    /**
     * Send message into user DM
     * @param {string} message : message to send to the user
     * @param {Array<Discord.ClientUser.id>} userIds (Only user in the cache's bot)
     */
    static async sendMessageIntoDM(message, userIds) {
        let users = []
        for (const id of userIds) {
            const user = await this.#client.users.cache.find(guild => guild.id === id)
            if (user)
                users.push(user)
        }
        // userIds.forEach(async id => users.push(await this.#client.users.cache.find(guild => guild.id === id).catch(err => {console.log(err.message); throw err})))
        if (users.length > 0) {
            for (const user of users)
                await user.createDM().then(DMChannel => DMChannel.send(message)).catch(err => {console.log(err.message); throw err})
        } else {
            const railo = this.#client.users.cache.find(guild => guild.id === config.USER_ID_RAILO)
            railo.createDM().then(DMChannel => DMChannel.send(
                "Une erreur est survenue lors de la récupération des utilisateur à l'aide des Ids.\nLe message était le suivant :\n\n" + message
            )).catch(err => {console.log(err.message); throw err})
        }
    }

    /**
     * Remove the value item of its array
     * @param {Array<any>} arr - The array from which we want delete the value
     * @param {any} value - The value we want to delete
     */
    static removeItemOnce(arr, value) {
        const index = arr.indexOf(value);
        if (index > -1) {
            arr.splice(index, 1);
        }
    }

    // /**
    //  * Remove item of its array
    //  * @param {Array<any>} arr - They array
    //  * @param {any} value - The item to delete
    //  */
    // static removeItem<T>(arr: Array<T>, value: T): Array<T> {
    //     const index = arr.indexOf(value);
    //     if (index > -1) {
    //         arr.splice(index, 1);
    //     }
    // }
}

module.exports = Tournament
