const config = require("../config.json")
const Discord = require("discord.js")

class SendLog {
    /**
     * Enum of type's log
     * @type {{INFO: string, SUCCESS: string, ERROR: string, WARNING: string}}
     */
    static level = {
        INFO: "ℹ Info ℹ",
        SUCCESS: "✅ Success ✅",
        WARNING: "⚠ Warning ⚠",
        ERROR: "❌ Error ❌",
    }

    /**
     *
     * @param {Discord.Client} client
     * @param {level.any} type
     * @param {string} message
     * @param {string} title
     */
    static async sendLog(client, type, message, title = "") {
        // If instances are OK
        if (!(client instanceof Discord.Client) || !(Object.values(this.level).includes(type)) || typeof message !== "string" || typeof title !== "string") {
            console.error("Error when sending logs")
            return
        }

        // Message to send
        const logToSend = `**${type}  ${title ? title : ""}** : \n${message}`

        // Send normal log
        // if (type === this.level.ERROR)
        //     console.error(message)
        // else
        //     console.log(message)

        // Send to the channel
        await client.channels.cache.get(config.CHANNEL_ID_log_bot).send(logToSend)

        // Send to Railo
        await client.users.cache.find(guild => guild.id === config.USER_ID_RAILO)
            .createDM()
            .then(DMChannel => DMChannel.send(logToSend))
            .catch(err => {console.log(err.message)})
    }
}

module.exports = SendLog
