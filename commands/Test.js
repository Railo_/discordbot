const config = require("../config.json")

class Test {

    /**
     * Object on event "message"
     */
    static #message

    /**
     * arguments sent by the author (after the command)
     */
    static #argument

    /**
     * arguments sent by the author (after the command)
     */
    static #exampleEmbed = {
        color: 0x0099ff,
        title: 'Some title',
        url: 'https://discord.js.org',
        author: {
            name: 'Some name',
            icon_url: 'https://i.imgur.com/wSTFkRM.png',
            url: 'https://discord.js.org',
        },
        description: 'Some description here',
        thumbnail: {
            url: 'https://i.imgur.com/wSTFkRM.png',
        },
        fields: [
            {
                name: 'Regular field title',
                value: 'Some value here',
            },
            {
                name: '\u200b',
                value: '\u200b',
                inline: false,
            },
            {
                name: 'Inline field title',
                value: 'Some value here',
                inline: true,
            },
            {
                name: 'Inline field title',
                value: 'Some value here',
                inline: true,
            },
            {
                name: 'Inline field title',
                value: 'Some value here',
                inline: true,
            },
        ],
        image: {
            url: 'https://i.imgur.com/wSTFkRM.png',
        },
        timestamp: new Date(),
        footer: {
            text: 'Some footer text here',
            icon_url: 'https://i.imgur.com/wSTFkRM.png',
        },
    };

    /**
     * Main function of Test class. Calls on test command
     * @param {Message} message - Object on event "message"
     * @param {[String]} args - Arguments on the user message
     */
    static test(message, args) {

        this.#message = message
        this.#argument = args

        if (String(message.author.id) === config.USER_ID_RAILO)
            this.guild()
        // If the user can't use this command (not having rights)
        else
            message.reply("Sorry you are enable to do this command").catch(err => console.log(err.message))
    }

    /**
     * To choose the Guild
     */
    static guild() {
        if (this.#message.guild.id === config.SERVER_ID_LYS) {
            this.guildLys()
        }
    }

    /**
     * For the LYS Guild
     */
    static guildLys() {
        if (this.#argument[0] === "test")
            this.#message.reply("TEST !").catch(err => console.log(err.message))

        else if (this.#argument[0] === "tournament")
            this.#message.channel.send({ embed : this.#exampleEmbed }).catch(err => console.log(err.message))

        else if (this.#argument[0] === "addrole") {
            const newChannel = this.#message.guild.channels.cache.find(role => role.id === config.CHANNEL_ID_registre_des_arrivees)
            this.#message.channel.send(newChannel.name)
        }
        // If there is at least one argument, but it is not valid
        else if (this.#argument[0])
            this.#message.channel.send("Arguments are not valid. Please add valid arguments.").catch(err => console.log(err.message))
        // If there is no argument
        else {
            this.#message.channel.send("This is a test command. Please add arguments.").catch(err => console.log(err.message))
            if (this.#argument.length > 0) {
                let i = 0
                this.#argument.forEach(arg => console.log(`Argument ${i+1} : ${arg}`))
            }
        }
    }
}

module.exports = Test
