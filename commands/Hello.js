const config = require("../config.json")
const Discord = require("discord.js")
const log = require("./SendLog")

class Hello {

    /**
     * The client of the bot
     * @type Discord.Client
     */
    static #client

    /**
     * Object on event "message"
     * @type Discord.Message
     */
    static #message

    /**
     * Message to send to the author
     * @type string
     */
    static #msg

    /**
     * Send a message and tag the author of the message
     * @param {Discord.Client} client
     * @param {string} message
     * @constructor
     */
    static async hello(client, message) {

        this.#client = client

        this.#message = message

        if (this.#message.guild.id === config.SERVER_ID_LYS) {
            this.guildLys()
        }
        else {
            this.#msg = "Hello !"
        }
        await this.reply()
    }

    /**
     * Reply to the author
     * @constructor
     */
    static async reply() {
        await this.#message.reply(this.#msg)
            .then(_ => {console.log(`Message "${this.#msg}" sent successfully to user "${this.#message.author.username}" !`); log.sendLog(this.#client, log.level.INFO, `Message "${this.#msg}" sent successfully to user "${this.#message.author.username}" !`) })
            .catch(err => {console.error(err.message); log.sendLog(this.#client, log.level.ERROR, err, "Sending Hello")})
    }

    /**
     * For the "LYS" guild
     * @constructor
     */
    static guildLys() {
        if (this.#message.member.id === config.USER_ID_RAILO) {
            this.#msg = " Bonjour !"
        }
        else if (this.#message.member.roles.cache.find(r => r.id === config.ROLE_ID_LYS_COMMANDEMENT)) {
            this.#msg = " Bonjour mon commandant !"
        }
        else if (this.#message.member.roles.cache.find(r => r.id === config.ROLE_ID_LYS_OFFICIER)) {
            this.#msg = " Bonjour maître officier !"
        }
        else if (this.#message.member.roles.cache.find(r => r.id === config.ROLE_ID_LYS_SERGENT)) {
            this.#msg = " Bonjour mon sergent !"
        }
        else if (this.#message.member.roles.cache.find(r => r.id === config.ROLE_ID_LYS_LYS)) {
            this.#msg = " Bonjour camarade !"
        }
        else {
            this.#msg = " Bonjour à toi !"
        }
    }
}

module.exports = Hello
